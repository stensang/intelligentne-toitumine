# README #

See dokument kirjeldab, kuidas saab intelligentsete süsteemide õppeaine projektifailid tööle.

### Mis selles repositooriumis on? ###

See repositoorium sisaldab TalTechi Intelligentsete süsteemide õppeaine raames loodud projektitöid.

## Kuidas käivitada? ##

Soovitav on repositooriumis olevate failide käivitamiseks luua pythoni virtuaalne keskkond
```
# Loo Pythoni virtuaalne keskkond
python3 -m venv venv

# Aktiveeri virtuaalne keskkond Linuxis
source venv/bin/activate
# Aktiveeri virtuaalne keskkond Windowsis
\venv\Scripts\activate.bat

# Vajalike teekide installimine virtuaalkeskkonnas
pip install -r requirements.txt
```

Seejärel saab huvipakkuvaid faile antud repositooriumist käivitada.
