import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import Perceptron, LogisticRegression
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import cross_val_score
from sklearn.tree import export_text

### Relatsiooniliste andmete sisselugemine ja kokkuliitmine

## Restaurants
# 1 chefmozaccepts.csv
# 2 chefmozcuisine.csv
chefmozCuisine = pd.read_csv('RCdata/chefmozcuisine.csv')
# 3 chefmozhours4.csv
# 4 chefmozparking.csv
chefmozParking = pd.read_csv('RCdata/chefmozparking.csv')
# 5 geoplaces2.csv
geoPlaces = pd.read_csv('RCdata/geoplaces2.csv')

## Consumers
# 6 usercuisine.csv
userCuisine = pd.read_csv('RCdata/usercuisine.csv')
# 7 userpayment.csv
# 8 userprofile.csv
userProfile = pd.read_csv('RCdata/userprofile.csv')

## User-Item-Rating
# 9 rating_final.csv
ratingFinal = pd.read_csv('RCdata/rating_final.csv')

# Relatsiooniliste andmete kokkuliitmine
observations = pd.merge(ratingFinal, userProfile, how='inner', on='userID')
observations = pd.merge(observations, userCuisine, how='inner', on='userID')
observations = pd.merge(observations, geoPlaces, how='inner', on='placeID')
observations = pd.merge(observations, chefmozParking, how='inner', on='placeID')
observations = pd.merge(observations, chefmozCuisine, how='inner', on='placeID')

# Andmete puhastamine ja ebaoluliste atribuutide eemaldamine
observations = observations.replace('�',0, regex=True)
observations = observations.loc[:, observations.columns.intersection(['hijos', 'height', 'interest', 'budget', 'transport', 'drink_level', 'weight',
                                                                    'alcohol', 'dress_code', 'franchise', 'accessibility', 'Rambience',
                                                                    'food_rating'])]

# Nominaalsed väärtused [0, 1] vastavustabeliks
observations = pd.get_dummies(observations)

# Prindi välja andmestiku suurus
print("\nAndmestikus on {} vaadet ja {} atribuuti".format(observations.shape[0], observations.shape[1]))

# Sisend ja väljundandmed
X = observations.drop(columns=['food_rating'])
y = observations['food_rating']
# X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4, random_state=0)

## Masinõppe meetodid
# predictive_model = DecisionTreeClassifier(max_depth=5)
# predictive_model = LogisticRegression(solver='lbfgs', max_iter=1000)
# predictive_model = Perceptron(max_iter=1000, tol=0.001, eta0=1.0, penalty='l1', random_state=0)
# predictive_model = GaussianNB()
predictive_model = KNeighborsClassifier(n_neighbors=7)

## Masinõppe mudelite treenimine
# predictive_model = predictive_model.fit(X_train,y_train)
# y_pred = predictive_model.predict(X_test)
predictive_model = predictive_model.fit(X,y)

## Treenitud mudel ennustuste täpsus
print("Mudel: \n", type(predictive_model).__name__)
# print("Täpsus:", accuracy_score(y_test, y_pred))
print("10-kordse ristvalideerimise keskmine täpsus: \n", np.average(cross_val_score(predictive_model, X, y, cv=10)))

## Olulisemad elemendid otsustuspuus
#print("")
#print(export_text(predictive_model, feature_names=list(X.columns)))
