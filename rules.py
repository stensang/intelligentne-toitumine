from durable.lang import *

# Idee on tagada, et päeva (edaspidi nädala, arvestades eelnevat) jooksul oleks kõik vajalikud vitamiinid ja muud komponendid toidust saadud.

with ruleset('food'):
    # Koguenergiavajadus
    ## Naised 18-20
    @when_all(
        (m.person.sex == 'female') & (m.person.age >= 18) & (m.person.age < 30) & (m.person.activity == 'low'))
    def kcal(c):
        c.assert_fact({'kcal': 1950})


    @when_all(
        (m.person.sex == 'female') & (m.person.age >= 18) & (m.person.age < 30) & (m.person.activity == 'moderate'))
    def kcal(c):
        c.assert_fact({'kcal': 2250})


    @when_all(
        (m.person.sex == 'female') & (m.person.age >= 18) & (m.person.age < 30) & (m.person.activity == 'high'))
    def kcal(c):
        c.assert_fact({'kcal': 2500})


    ## Naised 31-60
    @when_all(
        (m.person.sex == 'female') & (m.person.age >= 31) & (m.person.age < 60) & (m.person.activity == 'low'))
    def kcal(c):
        c.assert_fact({'kcal': 1800})


    @when_all(
        (m.person.sex == 'female') & (m.person.age >= 31) & (m.person.age < 60) & (m.person.activity == 'moderate'))
    def kcal(c):
        c.assert_fact({'kcal': 2050})


    @when_all(
        (m.person.sex == 'female') & (m.person.age >= 31) & (m.person.age < 60) & (m.person.activity == 'high'))
    def kcal(c):
        c.assert_fact({'kcal': 2350})


    ## Naised 61-74
    @when_all(
        (m.person.sex == 'female') & (m.person.age >= 60) & (m.person.age < 74) & (m.person.activity == 'low'))
    def kcal(c):
        c.assert_fact({'kcal': 1700})


    @when_all(
        (m.person.sex == 'female') & (m.person.age >= 60) & (m.person.age < 74) & (m.person.activity == 'moderate'))
    def kcal(c):
        c.assert_fact({'kcal': 1900})


    @when_all(
        (m.person.sex == 'female') & (m.person.age >= 60) & (m.person.age < 74) & (m.person.activity == 'high'))
    def kcal(c):
        c.assert_fact({'kcal': 2150})


    # Naised 74 ja üle
    @when_all((m.person.sex == 'female') & (m.person.age >= 74) & (m.person.activity == 'low'))
    def kcal(c):
        c.assert_fact({'kcal': 1600})


    @when_all((m.person.sex == 'female') & (m.person.age >= 74) & (m.person.activity == 'moderate'))
    def kcal(c):
        c.assert_fact({'kcal': 1800})


    @when_all((m.person.sex == 'female') & (m.person.age >= 74) & (m.person.activity == 'high'))
    def kcal(c):
        c.assert_fact({'kcal': 2000})



    ## Mehed 18-20
    @when_all((m.person.sex == 'male') & (m.person.age >= 18) & (m.person.age < 30) & (m.person.activity == 'low'))
    def kcal(c):
        c.assert_fact({'kcal': 2450})


    @when_all(
        (m.person.sex == 'male') & (m.person.age >= 18) & (m.person.age < 30) & (m.person.activity == 'moderate'))
    def kcal(c):
        c.assert_fact({'kcal': 2800})


    @when_all((m.person.sex == 'male') & (m.person.age >= 18) & (m.person.age < 30) & (m.person.activity == 'high'))
    def kcal(c):
        c.assert_fact({'kcal': 3150})


    ## Mehed 31-60
    @when_all((m.person.sex == 'male') & (m.person.age >= 30) & (m.person.age < 60) & (m.person.activity == 'low'))
    def kcal(c):
        c.assert_fact({'kcal': 2300})


    @when_all(
        (m.person.sex == 'male') & (m.person.age >= 30) & (m.person.age < 60) & (m.person.activity == 'moderate'))
    def kcal(c):
        c.assert_fact({'kcal': 2650})


    @when_all((m.person.sex == 'male') & (m.person.age >= 30) & (m.person.age < 60) & (m.person.activity == 'high'))
    def kcal(c):
        c.assert_fact({'kcal': 2950})


    ## Mehed 61-74
    @when_all((m.person.sex == 'male') & (m.person.age >= 60) & (m.person.age < 74) & (m.person.activity == 'low'))
    def kcal(c):
        c.assert_fact({'kcal': 2050})


    @when_all(
        (m.person.sex == 'male') & (m.person.age >= 60) & (m.person.age < 74) & (m.person.activity == 'moderate'))
    def kcal(c):
        c.assert_fact({'kcal': 2300})


    @when_all((m.person.sex == 'male') & (m.person.age >= 60) & (m.person.age < 74) & (m.person.activity == 'high'))
    def kcal(c):
        c.assert_fact({'kcal': 2600})


    # Mehed 74 ja üle
    @when_all((m.person.sex == 'male') & (m.person.age >= 74) & (m.person.activity == 'low'))
    def kcal(c):
        c.assert_fact({'kcal': 2000})


    @when_all((m.person.sex == 'male') & (m.person.age >= 74) & (m.person.activity == 'moderate'))
    def kcal(c):
        c.assert_fact({'kcal': 2200})


    @when_all((m.person.sex == 'male') & (m.person.age >= 74) & (m.person.activity == 'high'))
    def kcal(c):
        c.assert_fact({'kcal': 2400})

    # Def kcal_extra
    @when_all(+m.kcal)
    def kcal_extra(c):
        c.assert_fact({'kcal_extra': 0})

    ## Kui on naine, lapseootel ja 3. trimester, tuleb päeva kohta lisa 200 kcal
    @when_all(m.person.pregnancy_trimester == 3)
    def kcal_extra(c):
        c.retract_fact({'kcal_extra': 0})
        c.assert_fact({'kcal_extra': 200})

    # Kaalu langetamise ja juurde võtmisega arvestamine
    ## Kui on naine, lapseootel ja soovib kaalu langetada või tõsta, siis peaks pöörduma arsti poole
    @when_all((+m.person.pregnancy_trimester) & ((m.person.weight_lose_gain_maintain=='lose') | (m.person.weight_lose_gain_maintain=="gain")))
    def kcal_extra(c):
        print("Consult your doctor!")

    @when_all((m.person.weight_lose_gain_maintain == 'lose') & (-m.person.pregnancy_trimester))
    def kcal_extra(c):
        c.retract_fact({'kcal_extra': 0})
        c.assert_fact({'kcal_extra': -500})

    @when_all((m.person.weight_lose_gain_maintain == 'gain') & (-m.person.pregnancy_trimester))
    def kcal_extra(c):
        c.retract_fact({'kcal_extra': 0})
        c.assert_fact({'kcal_extra': 500})

    @when_all(c.first << (+m.kcal), c.second << (+m.kcal_extra))
    def kcal_all(c):
        c.assert_fact({'kcal_all': c.first.kcal + c.second.kcal_extra})


    # Energia jaotumine toidukordadele
    ## Hommikusöök
    @when_all(c.first << (m.schedule == 'normal') & (m.meal == 'breakfast'), c.second << (+m.kcal_all))
    def mealKcal(c):
        c.assert_fact({'mealKcal': c.second.kcal_all * 0.25})


    ## Lõunasöök
    @when_all(c.first << (m.schedule == 'normal') & (m.meal == 'lunch'), c.second << (+m.kcal_all))
    def mealKcal(c):
        c.assert_fact({'mealKcal': c.second.kcal_all * 0.35})


    ## Õhtusöök
    @when_all(c.first << (m.schedule == 'normal') & (m.meal == 'dinner'), c.second << (+m.kcal_all))
    def mealKcal(c):
        c.assert_fact({'mealKcal': c.second.kcal_all * 0.30})


    # Energia jaotumine toidukordadele erinevate töörežiimide korral
    ## Varajase vahetuse korral
    ### Hommikusöök
    @when_all(c.first << (m.schedule == 'early') & (m.meal == 'breakfast'), c.second << (+m.kcal_all))
    def mealKcal(c):
        c.assert_fact({'mealKcal': c.second.kcal_all * 0.35})


    ### Lõunasöök
    @when_all(c.first << (m.schedule == 'early') & (m.meal == 'lunch'), c.second << (+m.kcal_all))
    def mealKcal(c):
        c.assert_fact({'mealKcal': c.second.kcal_all * 0.35})


    ### Õhtusöök
    @when_all(c.first << (m.schedule == 'early') & (m.meal == 'dinner'), c.second << (+m.kcal_all))
    def mealKcal(c):
        c.assert_fact({'mealKcal': c.second.kcal_all * 0.25})


    ## Öövahetuse korral
    ### Hommikusöök enne magama minemist
    @when_all((m.schedule == 'late') & (m.meal == 'breakfast'))
    def mealKcal(c):
        c.assert_fact({'mealKcal': 200})


    ### Lõunasöök
    @when_all(c.first << (m.schedule == 'late') & (m.meal == 'lunch'), c.second << (+m.kcal_all))
    def mealKcal(c):
        c.assert_fact({'mealKcal': c.second.kcal_all * 0.35})


    ### Õhtusöök
    @when_all(c.first << (m.schedule == 'late') & (m.meal == 'dinner'), c.second << (+m.kcal_all))
    def mealKcal(c):
        c.assert_fact({'mealKcal': c.second.kcal_all * 0.35})


    # Makrotoitained
    @when_all(+m.mealKcal)
    def mealMacro(c):
        # Valgud
        c.assert_fact({'protein': (c.m.mealKcal * 0.10, c.m.mealKcal * 0.20)})
        # Rasvad
        c.assert_fact({'fat': (c.m.mealKcal * 0.25, c.m.mealKcal * 0.35)})
        # Süsivesikud
        c.assert_fact({'carbohydrates': (c.m.mealKcal * 0.50, c.m.mealKcal * 0.60)})
        # Kiudainete optimaalne soovitus
        c.assert_fact({'minFibers': 13 * (c.m.mealKcal / 1000)})
        # Sh maksimaalsed küllastunud rasvhapped
        c.assert_fact({'maxSFA': c.m.mealKcal * 0.10})
        # Sh omega-3
        c.assert_fact({'minOmega3': c.m.mealKcal * 0.01})


    ## Valguvajadus eakatel (>65-aastased) 15–20 %E
    @when_all(c.first << (m.person.age > 65), c.second << (+m.mealKcal))
    def mealMacro(c):
        # Valgud
        c.retract_fact({'protein': (c.second.mealKcal * 0.10, c.second.mealKcal * 0.20)})
        c.assert_fact({'protein': (c.second.mealKcal * 0.15, c.second.mealKcal * 0.20)})


    ## Toiduenergia vähenemisel alla 1900 kcal/päevas peavad valgud andma soovitusliku maksimumi %E
    @when_all((m.kcal_all < 1900), c.second << (+m.mealKcal))
    def mealMacro(c):
        # Valgud
        c.retract_fact({'protein': (c.m.mealKcal * 0.10, c.m.mealKcal * 0.20)})
        c.retract_fact({'protein': (c.second.mealKcal * 0.15, c.second.mealKcal * 0.20)})
        c.assert_fact({'protein': (c.second.mealKcal * 0.20, c.second.mealKcal * 0.20)})


    ## Kui high_blood_pressure=yes ja vanus > 40 siis soovita kiudaineterikkaid toite nagu puuviljad, marjad, köögiviljad
    @when_all((m.person.age > 40) & (m.person.high_blood_pressure == 'yes'))
    def mealSuggestVegs(c):
        c.assert_fact({'suggestVegetables': 'yes'})


    ## Vastused
    @when_all(+m.kcal)
    def kcal(c):
        print('dayKcal: {0}'.format(c.m.kcal))

    @when_all(+m.kcal_extra)
    def kcal_extra(c):
        print('dayKcalExtra: {0}'.format(c.m.kcal_extra))

    @when_all(+m.mealKcal)
    def mealKcal(c):
        print('mealKcal: {0}'.format(round(c.m.mealKcal),0))

    @when_all(+m.protein)
    def protein(c):
        prot = (round(c.m.protein[0],0), round(c.m.protein[1],0))
        print('protein: {0}'.format(prot))

    @when_all(+m.fat)
    def fat(c):
        fats = (round(c.m.fat[0],0), round(c.m.fat[1],0))
        print('fat: {0}'.format(fats))

    @when_all(+m.carbohydrates)
    def carbohydrates(c):
        carb = (round(c.m.carbohydrates[0],0), round(c.m.carbohydrates[1],0))
        print('carbohydrates: {0}'.format(carb))

    @when_all(+m.minFibers)
    def minfibers(c):
        print('minFibers: {0}'.format(round(c.m.minFibers),0))

    @when_all(+m.maxSFA)
    def maxSFA(c):
        print('maxSFA: {0}'.format(round(c.m.maxSFA),0))

    @when_all(+m.minOmega3)
    def minOmega3(c):
        print('minOmega3: {0}'.format(round(c.m.minOmega3),0))

    @when_all(+m.suggestVegetables)
    def mealSuggestVegs(c):
        print('suggestVegetables: {0}'.format(c.m.suggestVegetables))


# Sisendid:
# sex: 'male', 'female'
# activity: 'low', 'moderate', 'high'
# schedule: 'early', 'normal', 'late'
# meal: 'breakfast', 'lunch', 'dinner'
# high_blood_pressure 'yes', 'no'
# weight_lose_gain_maintain: 'lose', 'gain'
# pregnancy: 'yes', 'no'
# pregnancy_trimester: 1,2,3


print("# First person")
# assert_fact('food', {'person': {'sex': 'female', 'age': 33, 'activity': 'moderate', 'high_blood_pressure': 'no', 'weight_lose_gain_maintain': 'lose'}, 'schedule': 'normal', 'meal': 'lunch'})
# assert_fact('food', {'person': {'sex': 'female', 'age': 59, 'activity': 'low', 'high_blood_pressure': 'no', 'weight_lose_gain_maintain': 'lose'}, 'schedule': 'early', 'meal': 'breakfast'})
assert_fact('food', {'person': {'sex': 'female', 'age': 33, 'activity': 'moderate', 'high_blood_pressure': 'no', 'weight_lose_gain_maintain': 'gain', 'pregnancy_trimester': 3}, 'schedule': 'normal', 'meal': 'lunch'})
# assert_fact('food', {'person': {'sex': 'female', 'age': 33, 'activity': 'moderate', 'high_blood_pressure': 'no', 'weight_lose_gain_maintain': 'lose', 'pregnancy_trimester': 3}, 'schedule': 'normal', 'meal': 'lunch'})
# assert_fact('food', {'person': {'sex': 'female', 'age': 40, 'activity': 'moderate', 'high_blood_pressure': 'yes'}, 'schedule': 'normal', 'meal': 'lunch'})
# assert_fact('food', {'person': {'sex': 'male', 'age': 41, 'activity': 'moderate', 'high_blood_pressure': 'yes'}, 'schedule': 'normal', 'meal': 'lunch'})
# assert_fact('food', {'person': {'sex': 'male', 'age': 30, 'activity': 'high'}, 'schedule': 'late', 'meal': 'dinner'})
# assert_fact('food', {'person': {'sex': 'female', 'age': 35, 'activity': 'low'}, 'schedule': 'early', 'meal': 'breakfast'})
# assert_fact('food', {'person': {'sex': 'male', 'age': 40, 'activity': 'moderate'}, 'schedule': 'normal', 'meal': 'lunch'})
# assert_fact('food', {'person': {'sex': 'female', 'age': 50, 'activity': 'high'}, 'schedule': 'late', 'meal': 'dinner'})
# assert_fact('food', {'person': {'sex': 'male', 'age': 60, 'activity': 'low'}, 'schedule': 'early', 'meal': 'breakfast'})
# assert_fact('food', {'person': {'sex': 'female', 'age': 70, 'activity': 'moderate'}, 'schedule': 'normal', 'meal': 'lunch'})